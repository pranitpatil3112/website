-- Copyright 2017--2020 The Tor Project
-- See LICENSE for licensing information

-- Table of all known flags, to match flag strings to bit positions in the flags
-- column in status_entries. If the number of known flags ever grows larger than
-- 31, we'll have to extend flag_id to accept values between 0 and 63 and alter
-- the flags column in status_entries from INTEGER to BIGINT. And if it grows
-- even larger, we'll have to do something even smarter.
CREATE TABLE flags (
  flag_id SMALLINT PRIMARY KEY CHECK (flag_id BETWEEN 0 AND 30),
  flag_string TEXT UNIQUE NOT NULL
);

-- Hard-wire the Guard and (Bad)Exit flags, so that we can implement
-- aggregate_ipv6() below without having to look up their IDs in the flags
-- table.
INSERT INTO flags (flag_id, flag_string) VALUES (0, 'Guard');
INSERT INTO flags (flag_id, flag_string) VALUES (1, 'Exit');
INSERT INTO flags (flag_id, flag_string) VALUES (2, 'BadExit');

-- Table of all known versions, either from platform lines in server descriptors
-- or recommended-server-versions lines in consensuses. Versions found in the
-- latter are marked as recommended, which enables us to only include major
-- versions in results that have been recommended at least once in a consensus.
CREATE TABLE versions (
  version_id SERIAL PRIMARY KEY,
  version_string TEXT UNIQUE NOT NULL,
  recommended BOOLEAN NOT NULL
);

-- Hard-wire a 0.1.0 version and a 0.1.1 version, because these major versions
-- were never recommended in a consensus, yet they are supposed to go into the
-- versions statistics.
INSERT INTO versions (version_string, recommended) VALUES ('0.1.0.17', TRUE);
INSERT INTO versions (version_string, recommended) VALUES ('0.1.1.26', TRUE);

-- Table of all known platforms from platform lines in server descriptors, that
-- is, without Tor software version information.
CREATE TABLE platforms (
  platform_id SERIAL PRIMARY KEY,
  platform_string TEXT UNIQUE NOT NULL
);

-- Table of all relevant parts contained in relay or bridge server descriptors.
-- We're not deleting from this table, because we can never be sure that we
-- won't import a previously missing status that we'll want to match against
-- existing server descriptors.
CREATE TABLE server_descriptors (
  descriptor_digest_sha1 BYTEA PRIMARY KEY,
  platform_id INTEGER REFERENCES platforms (platform_id),
  version_id INTEGER REFERENCES versions (version_id),
  advertised_bandwidth_bytes INTEGER NOT NULL,
  announced_ipv6 BOOLEAN NOT NULL,
  exiting_ipv6_relay BOOLEAN NOT NULL
);

-- Enumeration type for servers, which can be either relays or bridges.
CREATE TYPE server_enum AS ENUM ('relay', 'bridge');

-- Table of all relay or bridge statuses. We're not deleting from this table.
CREATE TABLE statuses (
  status_id SERIAL PRIMARY KEY,
  server server_enum NOT NULL,
  valid_after TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  running_count INTEGER NOT NULL,
  consensus_weight_sum REAL,
  guard_weight_sum REAL,
  middle_weight_sum REAL,
  exit_weight_sum REAL,
  UNIQUE (server, valid_after)
);

-- Table of relay or bridge status entries. Unlike previous tables, we're
-- deleting from this table after aggregating rows into the aggregated table.
-- Otherwise this table would grow too large over time.
CREATE TABLE status_entries (
  status_id INTEGER REFERENCES statuses (status_id) NOT NULL,
  descriptor_digest_sha1 BYTEA NOT NULL,
  flags INTEGER NOT NULL,
  reachable_ipv6_relay BOOLEAN NOT NULL,
  consensus_weight REAL,
  guard_weight REAL,
  middle_weight REAL,
  exit_weight REAL,
  UNIQUE (status_id, descriptor_digest_sha1)
);

-- Table of joined and aggregated server_descriptors and status_entries rows.
-- For a given status and combination of flags and IPv6 capabilities, we count
-- the number of servers and advertised bandwidth bytes.
CREATE TABLE aggregated_ipv6 (
  status_id INTEGER REFERENCES statuses (status_id) NOT NULL,
  guard_relay BOOLEAN NOT NULL,
  exit_relay BOOLEAN NOT NULL,
  announced_ipv6 BOOLEAN NOT NULL,
  exiting_ipv6_relay BOOLEAN NOT NULL,
  reachable_ipv6_relay BOOLEAN NOT NULL,
  server_count_sum INTEGER NOT NULL,
  consensus_weight REAL,
  guard_weight REAL,
  middle_weight REAL,
  exit_weight REAL,
  advertised_bandwidth_bytes_sum BIGINT NOT NULL,
  CONSTRAINT aggregated_ipv6_unique
    UNIQUE (status_id, guard_relay, exit_relay, announced_ipv6,
    exiting_ipv6_relay, reachable_ipv6_relay)
);

-- Table of joined and aggregated server_descriptors and status_entries rows by
-- relay flag.
CREATE TABLE aggregated_flags (
  status_id INTEGER REFERENCES statuses (status_id) NOT NULL,
  flag_id INTEGER REFERENCES flags (flag_id) NOT NULL,
  server_count_sum INTEGER NOT NULL,
  consensus_weight REAL,
  guard_weight REAL,
  middle_weight REAL,
  exit_weight REAL,
  advertised_bandwidth_bytes_sum BIGINT NOT NULL,
  CONSTRAINT aggregated_flags_unique UNIQUE (status_id, flag_id)
);

-- Table of joined and aggregated server_descriptors and status_entries rows by
-- version.
CREATE TABLE aggregated_versions (
  status_id INTEGER REFERENCES statuses (status_id) NOT NULL,
  version_id INTEGER REFERENCES versions (version_id),
  server_count_sum INTEGER NOT NULL,
  consensus_weight REAL,
  guard_weight REAL,
  middle_weight REAL,
  exit_weight REAL,
  advertised_bandwidth_bytes_sum BIGINT NOT NULL,
  CONSTRAINT aggregated_versions_unique UNIQUE (status_id, version_id)
);

-- Table of joined and aggregated server_descriptors and status_entries rows by
-- platform.
CREATE TABLE aggregated_platforms (
  status_id INTEGER REFERENCES statuses (status_id) NOT NULL,
  platform_id INTEGER REFERENCES platforms (platform_id),
  server_count_sum INTEGER NOT NULL,
  consensus_weight REAL,
  guard_weight REAL,
  middle_weight REAL,
  exit_weight REAL,
  advertised_bandwidth_bytes_sum BIGINT NOT NULL,
  CONSTRAINT aggregated_platforms_unique UNIQUE (status_id, platform_id)
);

-- Function to aggregate server_descriptors and status_entries rows into the
-- aggregated_* tables and delete rows from status_entries that are then
-- contained in the aggregated_* tables. This function is supposed to be called
-- once after inserting new rows into server_descriptors and/or status_entries.
-- Subsequent calls won't have any effect.
CREATE OR REPLACE FUNCTION aggregate() RETURNS VOID AS $$
-- Aggregate by IPv6 support.
INSERT INTO aggregated_ipv6
SELECT status_id, flags & 1 > 0 AS guard_relay,
  flags & 2 > 0 AND flags & 4 = 0 AS exit_relay,
  announced_ipv6, exiting_ipv6_relay,
  reachable_ipv6_relay, COUNT(*) AS server_count_sum,
  SUM(consensus_weight) AS consensus_weight,
  SUM(guard_weight) AS guard_weight,
  SUM(middle_weight) AS middle_weight,
  SUM(exit_weight) AS exit_weight,
  SUM(advertised_bandwidth_bytes) AS advertised_bandwidth_bytes_sum
FROM status_entries
NATURAL JOIN server_descriptors
GROUP BY status_id, guard_relay, exit_relay, announced_ipv6, exiting_ipv6_relay,
  reachable_ipv6_relay
ON CONFLICT ON CONSTRAINT aggregated_ipv6_unique
DO UPDATE SET server_count_sum = aggregated_ipv6.server_count_sum
    + EXCLUDED.server_count_sum,
  consensus_weight = aggregated_ipv6.consensus_weight
    + EXCLUDED.consensus_weight,
  guard_weight = aggregated_ipv6.guard_weight + EXCLUDED.guard_weight,
  middle_weight = aggregated_ipv6.middle_weight + EXCLUDED.middle_weight,
  exit_weight = aggregated_ipv6.exit_weight + EXCLUDED.exit_weight,
  advertised_bandwidth_bytes_sum
    = aggregated_ipv6.advertised_bandwidth_bytes_sum
    + EXCLUDED.advertised_bandwidth_bytes_sum;
-- Aggregate by assigned relay flags.
INSERT INTO aggregated_flags
SELECT status_id, flag_id, COUNT(*) AS server_count_sum,
  SUM(consensus_weight) AS consensus_weight,
  SUM(guard_weight) AS guard_weight,
  SUM(middle_weight) AS middle_weight,
  SUM(exit_weight) AS exit_weight,
  SUM(advertised_bandwidth_bytes) AS advertised_bandwidth_bytes_sum
FROM status_entries
NATURAL JOIN server_descriptors
JOIN flags ON flags & (1 << flag_id) > 0
GROUP BY status_id, flag_id
ON CONFLICT ON CONSTRAINT aggregated_flags_unique
DO UPDATE SET server_count_sum = aggregated_flags.server_count_sum
    + EXCLUDED.server_count_sum,
  consensus_weight = aggregated_flags.consensus_weight
    + EXCLUDED.consensus_weight,
  guard_weight = aggregated_flags.guard_weight + EXCLUDED.guard_weight,
  middle_weight = aggregated_flags.middle_weight + EXCLUDED.middle_weight,
  exit_weight = aggregated_flags.exit_weight + EXCLUDED.exit_weight,
  advertised_bandwidth_bytes_sum
    = aggregated_flags.advertised_bandwidth_bytes_sum
    + EXCLUDED.advertised_bandwidth_bytes_sum;
-- Aggregate by version.
INSERT INTO aggregated_versions
SELECT status_id, version_id, COUNT(*) AS server_count_sum,
  SUM(consensus_weight) AS consensus_weight,
  SUM(guard_weight) AS guard_weight,
  SUM(middle_weight) AS middle_weight,
  SUM(exit_weight) AS exit_weight,
  SUM(advertised_bandwidth_bytes) AS advertised_bandwidth_bytes_sum
FROM status_entries
NATURAL JOIN server_descriptors
GROUP BY status_id, version_id
ON CONFLICT ON CONSTRAINT aggregated_versions_unique
DO UPDATE SET server_count_sum = aggregated_versions.server_count_sum
    + EXCLUDED.server_count_sum,
  consensus_weight = aggregated_versions.consensus_weight
    + EXCLUDED.consensus_weight,
  guard_weight = aggregated_versions.guard_weight + EXCLUDED.guard_weight,
  middle_weight = aggregated_versions.middle_weight + EXCLUDED.middle_weight,
  exit_weight = aggregated_versions.exit_weight + EXCLUDED.exit_weight,
  advertised_bandwidth_bytes_sum
    = aggregated_versions.advertised_bandwidth_bytes_sum
    + EXCLUDED.advertised_bandwidth_bytes_sum;
-- Aggregate by platform.
INSERT INTO aggregated_platforms
SELECT status_id, platform_id, COUNT(*) AS server_count_sum,
  SUM(consensus_weight) AS consensus_weight,
  SUM(guard_weight) AS guard_weight,
  SUM(middle_weight) AS middle_weight,
  SUM(exit_weight) AS exit_weight,
  SUM(advertised_bandwidth_bytes) AS advertised_bandwidth_bytes_sum
FROM status_entries
NATURAL JOIN server_descriptors
GROUP BY status_id, platform_id
ON CONFLICT ON CONSTRAINT aggregated_platforms_unique
DO UPDATE SET server_count_sum = aggregated_platforms.server_count_sum
    + EXCLUDED.server_count_sum,
  consensus_weight = aggregated_platforms.consensus_weight
    + EXCLUDED.consensus_weight,
  guard_weight = aggregated_platforms.guard_weight + EXCLUDED.guard_weight,
  middle_weight = aggregated_platforms.middle_weight + EXCLUDED.middle_weight,
  exit_weight = aggregated_platforms.exit_weight + EXCLUDED.exit_weight,
  advertised_bandwidth_bytes_sum
    = aggregated_platforms.advertised_bandwidth_bytes_sum
    + EXCLUDED.advertised_bandwidth_bytes_sum;
-- Delete obsolete rows from the status_entries table.
DELETE FROM status_entries WHERE EXISTS (
  SELECT 1 FROM server_descriptors
  WHERE descriptor_digest_sha1 = status_entries.descriptor_digest_sha1);
$$ LANGUAGE SQL;

-- Materialized views on previously aggregated IPv6 server statistics in a format that is
-- compatible for writing to an output CSV file. Statuses are only included in
-- the output if they have at least 1 relay or bridge with the Running flag and
-- if at least 99.9% of referenced server descriptors are present. Dates are
-- only included in the output if at least 12 statuses are known. The last two
-- dates are excluded to avoid statistics from flapping if missing descriptors
-- are provided late.
CREATE MATERIALIZED VIEW included_statuses_ipv6 AS (
SELECT statuses.status_id,
       statuses.server,
       statuses.valid_after,
       date(statuses.valid_after) as valid_after_date
FROM statuses JOIN aggregated_ipv6 USING (status_id)
GROUP BY statuses.status_id, statuses.server, statuses.valid_after
HAVING statuses.running_count > 0 AND (1000 * sum(aggregated_ipv6.server_count_sum)) > (999 * statuses.running_count));

CREATE MATERIALIZED VIEW included_dates_ipv6 AS (
SELECT included_statuses_ipv6.valid_after_date,
       included_statuses_ipv6.server
FROM included_statuses_ipv6
GROUP BY included_statuses_ipv6.valid_after_date, included_statuses_ipv6.server
HAVING count(included_statuses_ipv6.status_id) >= 12 AND included_statuses_ipv6.valid_after_date < (
  SELECT max(included_statuses_ipv6.valid_after_date) AS max
  FROM included_statuses_ipv6));

CREATE MATERIALIZED VIEW grouped_by_status_ipv6 AS (
SELECT statuses.valid_after,
       statuses.server,
       CASE
           WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.guard_relay
           ELSE NULL::boolean
       END AS guard_relay_or_null,
       CASE
           WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.exit_relay
           ELSE NULL::boolean
       END AS exit_relay_or_null,
       aggregated_ipv6.announced_ipv6,
       CASE
           WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.exiting_ipv6_relay
           ELSE NULL::boolean
       END AS exiting_ipv6_relay_or_null,
       CASE
           WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.reachable_ipv6_relay
           ELSE NULL::boolean
       END AS reachable_ipv6_relay_or_null,
       sum(aggregated_ipv6.server_count_sum) AS server_count_sum,
       CASE
           WHEN statuses.server = 'relay'::server_enum THEN sum(aggregated_ipv6.advertised_bandwidth_bytes_sum)
           ELSE NULL::numeric
       END AS advertised_bandwidth_bytes_sum
FROM statuses JOIN aggregated_ipv6 USING (status_id)
WHERE (statuses.status_id IN (
    SELECT included_statuses_ipv6.status_id
    FROM included_statuses_ipv6))
AND (date(statuses.valid_after) IN (
        SELECT included_dates_ipv6.valid_after_date
        FROM included_dates_ipv6
        WHERE included_dates_ipv6.server = statuses.server))
GROUP BY statuses.status_id, statuses.valid_after, statuses.server, (
         CASE
             WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.guard_relay
             ELSE NULL::boolean
         END), (
         CASE
             WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.exit_relay
             ELSE NULL::boolean
         END),
         aggregated_ipv6.announced_ipv6, (
         CASE
             WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.exiting_ipv6_relay
             ELSE NULL::boolean
         END), (
         CASE
             WHEN statuses.server = 'relay'::server_enum THEN aggregated_ipv6.reachable_ipv6_relay
             ELSE NULL::boolean
         END)
);

CREATE MATERIALIZED VIEW ipv6servers AS (
SELECT date(grouped_by_status_ipv6.valid_after) AS valid_after_date,
   grouped_by_status_ipv6.server,
   grouped_by_status_ipv6.guard_relay_or_null AS guard_relay,
   grouped_by_status_ipv6.exit_relay_or_null AS exit_relay,
   grouped_by_status_ipv6.announced_ipv6,
   grouped_by_status_ipv6.exiting_ipv6_relay_or_null AS exiting_ipv6_relay,
   grouped_by_status_ipv6.reachable_ipv6_relay_or_null AS reachable_ipv6_relay,
   floor(avg(grouped_by_status_ipv6.server_count_sum)) AS server_count_sum_avg,
   floor(avg(grouped_by_status_ipv6.advertised_bandwidth_bytes_sum)) AS advertised_bandwidth_bytes_sum_avg
FROM grouped_by_status_ipv6
GROUP BY (date(grouped_by_status_ipv6.valid_after)),
         grouped_by_status_ipv6.server,
         grouped_by_status_ipv6.guard_relay_or_null,
         grouped_by_status_ipv6.exit_relay_or_null,
         grouped_by_status_ipv6.announced_ipv6,
         grouped_by_status_ipv6.exiting_ipv6_relay_or_null,
         grouped_by_status_ipv6.reachable_ipv6_relay_or_null
ORDER BY (date(grouped_by_status_ipv6.valid_after)),
         grouped_by_status_ipv6.server,
         grouped_by_status_ipv6.guard_relay_or_null,
         grouped_by_status_ipv6.exit_relay_or_null,
         grouped_by_status_ipv6.announced_ipv6,
         grouped_by_status_ipv6.exiting_ipv6_relay_or_null,
         grouped_by_status_ipv6.reachable_ipv6_relay_or_null);

-- Materialized view on advertised bandwidth by Exit/Guard flag combination.
CREATE MATERIALIZED VIEW bandwidth_advbw AS (
SELECT ipv6servers.valid_after_date AS date,
       ipv6servers.exit_relay AS isexit,
       ipv6servers.guard_relay AS isguard,
       floor(sum(ipv6servers.advertised_bandwidth_bytes_sum_avg)) AS advbw
FROM ipv6servers
WHERE (ipv6servers.server = 'relay'::server_enum)
GROUP BY ipv6servers.valid_after_date, ipv6servers.exit_relay, ipv6servers.guard_relay
ORDER BY ipv6servers.valid_after_date, ipv6servers.exit_relay, ipv6servers.guard_relay);

-- Materialized Views on the number of running servers by relay flag.
CREATE MATERIALIZED VIEW included_statuses_flags AS (
SELECT statuses.status_id,
       statuses.server,
       statuses.valid_after,
       date(statuses.valid_after) as valid_after_date
FROM statuses
JOIN aggregated_flags USING (status_id)
GROUP BY statuses.status_id
HAVING statuses.running_count > 0 AND (1000 * sum(aggregated_flags.server_count_sum)) > (999 * statuses.running_count)
);

CREATE MATERIALIZED VIEW included_dates_flags AS (
SELECT included_statuses_flags.valid_after_date,
       included_statuses_flags.server
FROM included_statuses_flags
JOIN aggregated_flags USING (status_id)
GROUP BY included_statuses_flags.valid_after_date, included_statuses_flags.server
HAVING count(included_statuses_flags.status_id) >= 12 AND included_statuses_flags.valid_after_date < (
    SELECT max(date(included_statuses_flags.valid_after)) AS max
    FROM included_statuses_flags));

CREATE MATERIALIZED VIEW statuses_flags AS (
SELECT date(statuses.valid_after) AS valid_after_date,
       statuses.status_id,
	     statuses.server,
	     flags.flag_string AS flag,
	     aggregated_flags.server_count_sum,
	     aggregated_flags.consensus_weight,
	     statuses.consensus_weight_sum,
	     aggregated_flags.guard_weight,
	     statuses.guard_weight_sum,
	     aggregated_flags.middle_weight,
	     statuses.middle_weight_sum,
	     aggregated_flags.exit_weight,
	     statuses.exit_weight_sum
FROM statuses
JOIN aggregated_flags USING (status_id)
JOIN flags USING (flag_id));

CREATE MATERIALIZED VIEW servers_flags_complete AS (
SELECT valid_after_date,
       server,
       flag,
       floor(avg(server_count_sum)) AS server_count_sum_avg,
       avg(consensus_weight / consensus_weight_sum) AS consensus_weight_fraction,
       avg(guard_weight / guard_weight_sum) AS guard_weight_fraction,
       avg(middle_weight / middle_weight_sum) AS middle_weight_fraction,
       avg(exit_weight / exit_weight_sum) AS exit_weight_fraction
FROM statuses_flags
WHERE (status_id IN (
    SELECT included_statuses_flags.status_id
    FROM included_statuses_flags)) AND (valid_after_date IN (
        SELECT included_dates_flags.valid_after_date
        FROM included_dates_flags
        WHERE included_dates_flags.server = server))
GROUP BY valid_after_date, server, flag
ORDER BY valid_after_date, server, flag
);

-- View on the number of running relays and bridges.
CREATE MATERIALIZED VIEW servers_networksize AS (
SELECT servers_flags_complete.valid_after_date AS date,
floor(avg(
    CASE
        WHEN servers_flags_complete.server = 'relay'::server_enum THEN servers_flags_complete.server_count_sum_avg
        ELSE NULL::numeric
    END)) AS relays,
floor(avg(
    CASE
        WHEN servers_flags_complete.server = 'bridge'::server_enum THEN servers_flags_complete.server_count_sum_avg
        ELSE NULL::numeric
    END)) AS bridges
FROM servers_flags_complete
WHERE servers_flags_complete.flag = 'Running'::text
GROUP BY servers_flags_complete.valid_after_date
ORDER BY servers_flags_complete.valid_after_date);

-- Materialized view on the number of running relays by relay flag.
CREATE MATERIALIZED VIEW servers_relayflags AS (
SELECT servers_flags_complete.valid_after_date AS date,
       servers_flags_complete.flag,
       servers_flags_complete.server_count_sum_avg AS relays
FROM servers_flags_complete
WHERE servers_flags_complete.server = 'relay'::server_enum
AND (servers_flags_complete.flag = ANY (ARRAY['Running'::text, 'Exit'::text, 'Fast'::text, 'Guard'::text, 'Stable'::text, 'HSDir'::text]))
ORDER BY servers_flags_complete.valid_after_date, servers_flags_complete.flag);

-- Materialized views on the number of running servers by version.
CREATE MATERIALIZED VIEW included_statuses_versions AS (
SELECT statuses.status_id,
       statuses.server,
       statuses.valid_after,
       date(statuses.valid_after) as valid_after_date
FROM statuses
JOIN aggregated_versions USING (status_id)
GROUP BY statuses.status_id, statuses.server, statuses.valid_after
HAVING statuses.running_count > 0 AND (1000 * sum(aggregated_versions.server_count_sum)) > (999 * statuses.running_count)
);

CREATE MATERIALIZED VIEW included_dates_versions AS (
SELECT included_statuses_versions.valid_after_date,
       included_statuses_versions.server
FROM included_statuses_versions
GROUP BY included_statuses_versions.valid_after_date, included_statuses_versions.server
HAVING count(included_statuses_versions.status_id) >= 12 AND included_statuses_versions.valid_after_date < (
    SELECT max(included_statuses_versions.valid_after_date) AS max
    FROM included_statuses_versions)
);

CREATE MATERIALIZED VIEW included_versions AS (
SELECT "substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text) AS version
FROM versions
WHERE versions.recommended IS TRUE
GROUP BY ("substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text))
);

CREATE MATERIALIZED VIEW grouped_by_version_complete AS (
SELECT statuses.server,
       statuses.valid_after,
       CASE
           WHEN ("substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text) IN (
               SELECT included_versions.version
               FROM included_versions)) THEN "substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text)
           ELSE 'Other'::text
       END AS version,
       sum(aggregated_versions.server_count_sum) AS server_count_sum,
       sum(aggregated_versions.consensus_weight) AS consensus_weight,
       sum(aggregated_versions.guard_weight) AS guard_weight,
       sum(aggregated_versions.middle_weight) AS middle_weight,
       sum(aggregated_versions.exit_weight) AS exit_weight,
       statuses.consensus_weight_sum,
       statuses.guard_weight_sum,
       statuses.middle_weight_sum,
       statuses.exit_weight_sum
FROM statuses
JOIN aggregated_versions USING (status_id)
LEFT JOIN versions ON aggregated_versions.version_id = versions.version_id
WHERE (statuses.status_id IN (
    SELECT included_statuses_versions.status_id
    FROM included_statuses_versions)) AND (date(statuses.valid_after) IN (
        SELECT included_dates_versions.valid_after_date
        FROM included_dates_versions
        WHERE included_dates_versions.server = statuses.server))
    GROUP BY statuses.status_id, statuses.server, statuses.valid_after, (
    CASE
        WHEN ("substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text) IN (
            SELECT included_versions.version
            FROM included_versions)) THEN "substring"(versions.version_string, '^([^\.]+\.[^\.]+\.[^\.]+)'::text)
        ELSE 'Other'::text
    END));

CREATE MATERIALIZED VIEW servers_versions_complete AS (
  SELECT date(grouped_by_version_complete.valid_after) AS valid_after_date,
   grouped_by_version_complete.server,
   grouped_by_version_complete.version,
   floor(avg(grouped_by_version_complete.server_count_sum)) AS server_count_sum_avg,
   avg(grouped_by_version_complete.consensus_weight / grouped_by_version_complete.consensus_weight_sum) AS consensus_weight_fraction,
   avg(grouped_by_version_complete.guard_weight / grouped_by_version_complete.guard_weight_sum) AS guard_weight_fraction,
   avg(grouped_by_version_complete.middle_weight / grouped_by_version_complete.middle_weight_sum) AS middle_weight_fraction,
   avg(grouped_by_version_complete.exit_weight / grouped_by_version_complete.exit_weight_sum) AS exit_weight_fraction
  FROM grouped_by_version_complete
 GROUP BY (date(grouped_by_version_complete.valid_after)), grouped_by_version_complete.server, grouped_by_version_complete.version
 ORDER BY (date(grouped_by_version_complete.valid_after)), grouped_by_version_complete.server, grouped_by_version_complete.version
);

CREATE MATERIALIZED VIEW grouped_by_version AS (
SELECT statuses.server,
       statuses.valid_after,
       date(statuses.valid_after) as valid_after_date,
           CASE
               WHEN versions.version_string ~~ '%-dev%'::text THEN 'dev'::text
               WHEN versions.version_string ~~ '%-rc%'::text THEN 'rc'::text
               WHEN versions.version_string ~~ '%-alpha%'::text THEN 'alpha'::text
               WHEN versions.version_string ~~ '%-beta%'::text THEN 'beta'::text
               ELSE 'stable'::text
           END AS version,
       sum(aggregated_versions.server_count_sum) AS server_count_sum,
       sum(aggregated_versions.consensus_weight) AS consensus_weight,
       sum(aggregated_versions.guard_weight) AS guard_weight,
       sum(aggregated_versions.middle_weight) AS middle_weight,
       sum(aggregated_versions.exit_weight) AS exit_weight,
       statuses.consensus_weight_sum,
       statuses.guard_weight_sum,
       statuses.middle_weight_sum,
       statuses.exit_weight_sum
FROM statuses
JOIN aggregated_versions USING (status_id)
LEFT JOIN versions ON aggregated_versions.version_id = versions.version_id
WHERE (statuses.status_id IN (
    SELECT included_statuses_versions.status_id
    FROM included_statuses_versions)) AND (date(statuses.valid_after) IN (
        SELECT included_dates_versions.valid_after_date
        FROM included_dates_versions
        WHERE included_dates_versions.server = statuses.server))
    GROUP BY statuses.status_id, statuses.server, statuses.valid_after, (
        CASE
            WHEN versions.version_string ~~ '%-dev%'::text THEN 'dev'::text
            WHEN versions.version_string ~~ '%-rc%'::text THEN 'rc'::text
            WHEN versions.version_string ~~ '%-alpha%'::text THEN 'alpha'::text
            WHEN versions.version_string ~~ '%-beta%'::text THEN 'beta'::text
            ELSE 'stable'::text
        END));

CREATE MATERIALIZED VIEW cw_versions AS (
SELECT date(grouped_by_version.valid_after) AS valid_after_date,
    grouped_by_version.server,
    grouped_by_version.version,
    floor(avg(grouped_by_version.server_count_sum)) AS server_count_sum_avg,
    avg(grouped_by_version.consensus_weight / grouped_by_version.consensus_weight_sum) AS consensus_weight_fraction,
    avg(grouped_by_version.guard_weight / grouped_by_version.guard_weight_sum) AS guard_weight_fraction,
    avg(grouped_by_version.middle_weight / grouped_by_version.middle_weight_sum) AS middle_weight_fraction,
    avg(grouped_by_version.exit_weight / grouped_by_version.exit_weight_sum) AS exit_weight_fraction
   FROM grouped_by_version
GROUP BY (date(grouped_by_version.valid_after)), grouped_by_version.server, grouped_by_version.version
ORDER BY (date(grouped_by_version.valid_after)), grouped_by_version.server, grouped_by_version.version);

-- View on the number of running relays by version.
CREATE MATERIALIZED VIEW servers_versions AS (
SELECT servers_versions_complete.valid_after_date AS date,
       servers_versions_complete.version,
       servers_versions_complete.server_count_sum_avg AS relays
FROM servers_versions_complete
WHERE servers_versions_complete.server = 'relay'::server_enum
ORDER BY servers_versions_complete.valid_after_date, servers_versions_complete.version);

-- Materialized views on the number of running servers by platform.
CREATE MATERIALIZED VIEW included_statuses_platforms AS (
SELECT statuses.status_id,
       statuses.server,
       statuses.valid_after,
       date(statuses.valid_after) as valid_after_date
FROM statuses
JOIN aggregated_platforms USING (status_id)
GROUP BY statuses.status_id, statuses.server, statuses.valid_after
HAVING statuses.running_count > 0 AND (1000 * sum(aggregated_platforms.server_count_sum)) > (999 * statuses.running_count)
);

CREATE MATERIALIZED VIEW included_dates_platforms AS (
SELECT included_statuses_platforms.valid_after_date,
       included_statuses_platforms.server
FROM included_statuses_platforms
GROUP BY (included_statuses_platforms.valid_after_date), included_statuses_platforms.server
HAVING count(included_statuses_platforms.status_id) >= 12 AND included_statuses_platforms.valid_after_date < (
    SELECT max(included_statuses_platforms.valid_after_date) AS max
    FROM included_statuses_platforms )
);

CREATE MATERIALIZED VIEW grouped_by_platform AS (
SELECT statuses.server,
       statuses.valid_after,
           CASE
               WHEN platforms.platform_string ~~ 'Linux%'::text THEN 'Linux'::text
               WHEN platforms.platform_string ~~ 'Windows%'::text THEN 'Windows'::text
               WHEN platforms.platform_string ~~ 'Darwin%'::text THEN 'macOS'::text
               WHEN platforms.platform_string ~~ '%BSD%'::text THEN 'BSD'::text
               ELSE 'Other'::text
           END AS platform,
       sum(aggregated_platforms.server_count_sum) AS server_count_sum,
       sum(aggregated_platforms.consensus_weight) AS consensus_weight,
       sum(aggregated_platforms.guard_weight) AS guard_weight,
       sum(aggregated_platforms.middle_weight) AS middle_weight,
       sum(aggregated_platforms.exit_weight) AS exit_weight,
       statuses.consensus_weight_sum,
       statuses.guard_weight_sum,
       statuses.middle_weight_sum,
       statuses.exit_weight_sum
FROM statuses
JOIN aggregated_platforms USING (status_id)
LEFT JOIN platforms ON aggregated_platforms.platform_id = platforms.platform_id
WHERE (statuses.status_id IN (
    SELECT included_statuses_platforms.status_id
    FROM included_statuses_platforms)) AND (date(statuses.valid_after) IN (
        SELECT included_dates_platforms.valid_after_date
        FROM included_dates_platforms
        WHERE included_dates_platforms.server = statuses.server))
GROUP BY statuses.status_id, statuses.server, statuses.valid_after, (
    CASE
        WHEN platforms.platform_string ~~ 'Linux%'::text THEN 'Linux'::text
        WHEN platforms.platform_string ~~ 'Windows%'::text THEN 'Windows'::text
        WHEN platforms.platform_string ~~ 'Darwin%'::text THEN 'macOS'::text
        WHEN platforms.platform_string ~~ '%BSD%'::text THEN 'BSD'::text
        ELSE 'Other'::text
    END)
);

CREATE MATERIALIZED VIEW servers_platforms_complete AS (
SELECT date(grouped_by_platform.valid_after) AS valid_after_date,
            grouped_by_platform.server,
            grouped_by_platform.platform,
            floor(avg(grouped_by_platform.server_count_sum)) AS server_count_sum_avg,
            avg(grouped_by_platform.consensus_weight / grouped_by_platform.consensus_weight_sum) AS consensus_weight_fraction,
            avg(grouped_by_platform.guard_weight / grouped_by_platform.guard_weight_sum) AS guard_weight_fraction,
            avg(grouped_by_platform.middle_weight / grouped_by_platform.middle_weight_sum) AS middle_weight_fraction,
            avg(grouped_by_platform.exit_weight / grouped_by_platform.exit_weight_sum) AS exit_weight_fraction
FROM grouped_by_platform
GROUP BY (date(grouped_by_platform.valid_after)), grouped_by_platform.server, grouped_by_platform.platform
ORDER BY (date(grouped_by_platform.valid_after)), grouped_by_platform.server, grouped_by_platform.platform);

-- View on the number of running relays by platform.
CREATE MATERIALIZED VIEW servers_platforms AS (
SELECT servers_platforms_complete.valid_after_date AS date,
       servers_platforms_complete.platform,
       servers_platforms_complete.server_count_sum_avg AS relays
FROM servers_platforms_complete
WHERE servers_platforms_complete.server = 'relay'::server_enum
ORDER BY servers_platforms_complete.valid_after_date, servers_platforms_complete.platform);
